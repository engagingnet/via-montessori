// The boilerplate for fontawesome

import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
Vue.component('fa', FontAwesomeIcon)

/* Libraries:
@fortawesome/fontawesome-free-brands fab
@fortawesome/fontawesome-pro-light fal
@fortawesome/fontawesome-pro-regular far
@fortawesome/fontawesome-pro-solid fas
*/

import { faChevronCircleUp  } from '@fortawesome/fontawesome-pro-solid'
library.add(faChevronCircleUp)

// import { faVuejs  } from '@fortawesome/fontawesome-free-brands'
// library.add(faVuejs)

// Vue.config.productionTip = false